# This multi-stage build has two steps.

# 1. Build TA-Lib (Technical Analysis Library)
FROM gcc:8.2 AS ta_build
COPY ./third_party/ta-lib /ta-lib
WORKDIR ta-lib
RUN mkdir -p usr/{include,bin} && \
    ./configure --prefix=/ta-lib/usr && \
    (make -j; make) && \
    make install

# 2. Build Python image, embedding built binary TA-Lib.
FROM python:3.7
COPY --from=ta_build /ta-lib/usr /usr
# Install Python packages.
# - The TA-Lib dependency specification is broken. We need to put it in
#   a separate requirements file.
# - Since chardet requires some binaries in system's PATH, we will
#   simplify the setup installing packages as system-wide. Hence,
#   package binaries go to /usr/local/bin.
COPY requirements_base.txt .
RUN pip install -r requirements_base.txt
COPY requirements_specific.txt .
RUN pip install -r requirements_specific.txt
# Create the ETL user.
RUN useradd -g users -m -s /sbin/nologin etl
WORKDIR /home/etl
# Copy code and service key.
COPY keys/ ./
RUN chown etl google-key.json && \
	chmod 400 google-key.json
#COPY *.py ./
#RUN chown etl *.py && \
#	chmod 400 *.py
USER etl
ENV GOOGLE_APPLICATION_CREDENTIALS="/home/etl/google-key.json"
CMD python sync.py
