"""Synchonization pipeline."""
import logging

from google.cloud import bigquery, storage

import api.bigquery
import api.storage
import b3


logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s [%(levelname)-7.7s] [%(name)s] %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

bq_client = bigquery.Client()
gcs_client = storage.Client()

bucket = api.storage.Bucket(name='pgd', client=gcs_client)

dataset = api.bigquery.Dataset(
    name='b3',
    description=(
        'Bovespa B3 (Brasil, Bolsa, Balcão S.A.) stock exchange '
        'with Equity, Option, and BM&F historical data.'
    ),
    labels={
        'manager': 'etilico',
        'environment': 'production',
        'source': 'ftp',
        'state': 'active',
        'component': 'ingest',
        'team': 'data',
    },
    client=bq_client,
    location='US'
)
dataset.sync()


stage_dataset = api.bigquery.Dataset(
    name='etilico_stage',
    description='Staging temporary tables',
    labels={
        'manager': 'etilico',
        'environment': 'production',
        'source': 'ftp',
        'state': 'active',
        'component': 'ingest',
        'team': 'data',
    },
    client=bq_client,
    location='US'
)
dataset.sync()

b3.stock.sync(bucket, dataset, stage_dataset)
b3.futures.sync(bucket, dataset, stage_dataset)

b3.time_bars.sync_stock(dataset)
b3.time_bars.sync_futures(dataset)

b3.tick_bars.sync_stock(dataset)
b3.tick_bars.sync_futures(dataset)

b3.brokers.sync(dataset)

b3.broker_time_bars.sync_stock(dataset)
b3.broker_time_bars.sync_futures(dataset)

b3.broker_tick_bars.sync_stock(dataset)
b3.broker_tick_bars.sync_futures(dataset)
