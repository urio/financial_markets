import logging

from google.cloud import storage

import api.storage
import b3


logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s [%(levelname)-7.7s] [%(name)s] %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

gcs_client = storage.Client()

bucket = api.storage.Bucket(name='pgd', client=gcs_client)

b3.cota_hist_files.sync(bucket)
