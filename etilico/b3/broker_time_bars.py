"""Brokers time bars."""
import logging

import api.bigquery
from . import schema
from .stock import get_stock_table
from .futures import get_futures_table


def insert_query(table_id, source_table_id, date, period_size, period_name):
    sql = f'''
INSERT INTO `{table_id}` (
           period, session_date, member, ticker, bar_time,
           volume, traded_quantity, count,
           buy_volume, buy_traded_quantity, buy_count,
           sell_volume, sell_traded_quantity, sell_count)
  WITH buy_stock AS (
SELECT session_date,
       buy_member AS member,
       ticker,
       trade_time,
       trade_price,
       traded_quantity
  FROM `{source_table_id}`
 WHERE session_date = DATE {date!r}),
       sell_stock AS (
SELECT session_date,
       sell_member AS member,
       ticker,
       trade_time,
       trade_price,
       traded_quantity
  FROM `{source_table_id}`
 WHERE session_date = DATE {date!r}),
       minmax AS (
SELECT TIME_TRUNC(MIN(trade_time), SECOND) AS min_time,
       TIME_TRUNC(MAX(trade_time), SECOND) AS max_time,
       {period_size} AS step
  FROM buy_stock),
       intervals AS (
SELECT TIME_ADD(min_time, INTERVAL step * num MINUTE) AS bar_begin_time,
       TIME_ADD(
           min_time,
           INTERVAL step * 60 * (1 + num) - 1 SECOND) AS bar_end_time
  FROM minmax,
       UNNEST(GENERATE_ARRAY(
           0, DIV(TIME_DIFF(max_time, min_time, MINUTE), step))) AS num),
       buy_bar AS (
SELECT {period_name!r} AS period,
       a.session_date,
       a.member,
       a.ticker,
       a.bar_begin_time AS bar_time,
       SUM(a.trade_price * a.traded_quantity) AS volume,
       SUM(a.traded_quantity) AS traded_quantity,
       COUNT(a.trade_time) AS count
  FROM (SELECT session_date,
               member,
               ticker,
               bar_begin_time,
               bar_end_time,
               trade_time,
               trade_price,
               traded_quantity
          FROM intervals
               JOIN
               buy_stock
               ON trade_time BETWEEN bar_begin_time AND bar_end_time
        WINDOW group_window AS (PARTITION BY session_date, member, ticker, bar_begin_time),
               asc_window AS (PARTITION BY session_date, member, ticker, bar_begin_time ORDER BY trade_time ASC),
               desc_window AS (PARTITION BY session_date, member, ticker, bar_begin_time ORDER BY trade_time DESC)) AS a
 GROUP BY a.session_date, a.member, a.ticker, a.bar_begin_time
 ORDER BY a.session_date, a.member, a.ticker, a.bar_begin_time),
       sell_bar AS (
SELECT {period_name!r} AS period,
       a.session_date,
       a.member,
       a.ticker,
       a.bar_begin_time AS bar_time,
       SUM(a.trade_price * a.traded_quantity) AS volume,
       SUM(a.traded_quantity) AS traded_quantity,
       COUNT(a.trade_time) AS count
  FROM (SELECT session_date,
               member,
               ticker,
               bar_begin_time,
               bar_end_time,
               trade_time,
               trade_price,
               traded_quantity
          FROM intervals
               JOIN
               sell_stock
               ON trade_time BETWEEN bar_begin_time AND bar_end_time
        WINDOW group_window AS (PARTITION BY session_date, member, ticker, bar_begin_time),
               asc_window AS (PARTITION BY session_date, member, ticker, bar_begin_time ORDER BY trade_time ASC),
               desc_window AS (PARTITION BY session_date, member, ticker, bar_begin_time ORDER BY trade_time DESC)) AS a
 GROUP BY a.session_date, a.member, a.ticker, a.bar_begin_time
 ORDER BY a.session_date, a.member, a.ticker, a.bar_begin_time),
       member_join AS (
SELECT COALESCE(b.period, s.period) AS period,
       COALESCE(b.session_date, s.session_date) AS session_date,
       COALESCE(b.member, s.member) AS member,
       COALESCE(b.ticker, s.ticker) AS ticker,
       COALESCE(b.bar_time, s.bar_time) AS bar_time,
       b.volume AS buy_volume,
       b.traded_quantity AS buy_traded_quantity,
       b.count AS buy_count,
       s.volume AS sell_volume,
       s.traded_quantity AS sell_traded_quantity,
       s.count AS sell_count
  FROM buy_bar AS b
       FULL OUTER JOIN
       sell_bar AS s
       ON b.period = s.period
       AND b.session_date = s.session_date
       AND b.member = s.member
       AND b.ticker = s.ticker
       AND b.bar_time = s.bar_time)
SELECT period,
       session_date,
       member,
       ticker,
       bar_time,
       COALESCE(buy_volume, 0) - COALESCE(sell_volume, 0) AS volume,
       COALESCE(buy_traded_quantity, 0)
       - COALESCE(sell_traded_quantity, 0) AS traded_quantity,
       COALESCE(buy_count, 0) - COALESCE(sell_count, 0) AS count,
       buy_volume,
       buy_traded_quantity,
       buy_count,
       sell_volume,
       sell_traded_quantity,
       sell_count
  FROM member_join
 ORDER BY session_date, member, ticker, bar_time;
    '''
    return sql


def sync(dataset, table, source_table):
    table_id = table.sql_table_id
    small_table_id = table.table_id
    source_table_id = source_table.sql_table_id
    client = table.client

    stock_dates = set(source_table.partitions())
    bar_dates = set(table.partitions())
    missing_dates = sorted(stock_dates - bar_dates)

    logger = logging.getLogger('b3.broker_time_bars')
    for date in missing_dates:
        date_str = date.strftime('%Y-%m-%d')
        logger.info(f'Processing {small_table_id} {date_str}')
        views = [
            (5, '5 minutes'),
            (15, '15 minutes'),
            (60, '60 minutes'),
            (720, '1 day'),
        ]
        for size, name in views:
            sql = insert_query(table_id, source_table_id, date_str, size, name)
            job = client.query(sql)
            job.result()


def sync_stock(dataset):
    table = api.bigquery.Table(
        name='stock_broker_time_bars',
        description=(
            'Historical Bovespa Stock Exchange broker trades time bars.\n'
            'Availability: D-2 trading days.\n'
            'Key: (period, session_date, member, ticker, bar_time).'
        ),
        labels={
            'source': 'b3',
            'environment': 'production',
            'component': 'ingest',
            'team': 'data',
            'state': 'active',
        },
        schema=schema.broker_time_bars.SCHEMA,
        partition='session_date',
        dataset=dataset
    )
    table.sync()
    source_table = get_stock_table(dataset)
    sync(dataset, table, source_table)


def sync_futures(dataset):
    table = api.bigquery.Table(
        name='futures_broker_time_bars',
        description=(
            'Historical Bovespa Futures Stock Exchange broker trades time bars.'
            'Availability: D-2 trading days.\n'
            'Key: (period, session_date, member, ticker, bar_time).'
        ),
        labels={
            'source': 'b3',
            'environment': 'production',
            'component': 'ingest',
            'team': 'data',
            'state': 'active',
        },
        schema=schema.broker_time_bars.SCHEMA,
        partition='session_date',
        dataset=dataset
    )
    table.sync()
    source_table = get_futures_table(dataset)
    sync(dataset, table, source_table)
