"""Reader of Bovespa B3 raw files."""
import logging
import tempfile
from zipfile import ZipFile

from pandas import read_csv

from . import schema


STOCK_COLS = [col.name for col in schema.stock.SCHEMA]
"""Column names in the stock table"""

TXT_COLUMNS = [col.name for col in schema.stock.FULL_SCHEMA
               if col.name != 'row_number']
"""Columns in the source file"""


def read_zip(blob, sep=';', names=None):
    # The zip file must contain exactly one file.
    # Example of file_list: ['apphmb/intraday/NEG_20160915.TXT']
    with tempfile.TemporaryFile() as tmpf:
        blob.download_to_file(tmpf)
        tmpf.seek(0)
        zip_file = ZipFile(tmpf)
        file_list = zip_file.namelist()
        assert len(file_list) == 1
        with zip_file.open(file_list[0]) as data_fp:
            return read_csv(data_fp, sep=sep, names=names, dtype=object)


def read_gz(blob, sep=';', names=None):
    # The zip file must contain exactly one file.
    # Example of file_list: ['apphmb/intraday/NEG_20160915.TXT']
    with tempfile.TemporaryFile() as tmpf:
        blob.download_to_file(tmpf)
        tmpf.seek(0)
        return read_csv(tmpf, sep=sep, names=names, dtype=object,
                        compression='gzip')


def read_stock(blob):
    logger = logging.getLogger('b3.reader')
    if blob.name.endswith('zip'):
        df = read_zip(blob, sep=';', names=TXT_COLUMNS)
    elif blob.name.endswith('gz'):
        df = read_gz(blob, sep=';', names=TXT_COLUMNS)
    col_number = len(TXT_COLUMNS)
    if df.shape[1] != col_number:
        logger.error(
            f'Error when reading input file: '
            f'expecting {col_number} columns, '
            f'read with {df.shape[1]} cols.'
        )
        raise ValueError('Wrong format')
    # Instead of using `skiprows` in pandas.read_table, we want
    # the index to reflect the original position in the input
    # file.
    df.drop(df.head(1).index, inplace=True)
    df.drop(df.tail(1).index, inplace=True)
    obj_cols = df.select_dtypes(include=[object]).columns
    for col in obj_cols:
        df[col] = df[col].str.replace(' ', '')

    df.index.name = 'row_number'
    df.reset_index(inplace=True)

    drop_rows = df.isna().any(axis=1).sum()
    if drop_rows > 0:
        cols_with_missing = ', '.join(df.columns[df.isna().any(axis=0)])
        if drop_rows > 100  :
            logger.error(f'Rows with missing values: {drop_rows:,d}')
            logger.error(f'Total number of rows: {df.shape[0]:,d}')
            logger.error(f'Columns with missing values: {cols_with_missing}')
            raise ValueError('Too many rows with missing values to drop.')
        logger.warning((
            f'{drop_rows:,d}/{df.shape[0]:,d} rows '
            f'({drop_rows/df.shape[0]:.2%}) were dropped due to '
            f'missing values. Index will be kept.'))
        df = df.dropna()
    return df
