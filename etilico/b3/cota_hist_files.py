"""Synchrozine files provided only by HTTP."""
import logging
import re
from datetime import datetime

from tempfile import TemporaryFile
from urllib.request import urlopen
from urllib.error import HTTPError
from shutil import copyfileobj


URL_PATH = 'http://bvmf.bmfbovespa.com.br/InstDados/SerHist'

BUCKET_PATH = 'datalake/stock_market/b3/http/InstDados/SerHist'
"""Bucket location of all stock files"""

ANNUAL_PATTERN = re.compile(r'^COTAHIST_A\d{4}\.ZIP$')

ANNUAL_FILE = '{path}/COTAHIST_A{year}.ZIP'


def sync(bucket):
    # http://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A2018.ZIP
    files = list(bucket.files(BUCKET_PATH, ANNUAL_PATTERN))
    print('files', files)
    if not files:
        start_year = 1986  # The earliest available year
    else:
        raise NotImplementedError

    logger = logging.getLogger('b3.cota_hist_files')

    current_year = datetime.today().year
    for year in range(start_year, current_year + 1):
        url_file = ANNUAL_FILE.format(path=URL_PATH, year=year)
        bucket_file = ANNUAL_FILE.format(path=BUCKET_PATH, year=year)

        try:
            with urlopen(url_file) as response, TemporaryFile() as file:
                logger.info(f'Processing {year}')
                copyfileobj(response, file)
                file.seek(0)
                blob = bucket.bucket.blob(bucket_file)
                blob.upload_from_file(file)
        except HTTPError as error:
            if error.code != 404:
                raise
