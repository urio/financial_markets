from google.cloud.bigquery import SchemaField


SCHEMA = [
    SchemaField(
        name='period',
        field_type='STRING',
        mode='REQUIRED',
        description=('Period of time for generating bars.')
    ),
    SchemaField(
        name='session_date',
        field_type='DATE',
        mode='REQUIRED',
        description='Session date. Format: YYYY-MM-dd. Example: 2018-09-20.'
    ),
    SchemaField(
        name='member',
        field_type='INT64',
        mode='REQUIRED',
        description='Entering Firm (Broker) - Available from March/2014.'
    ),
    SchemaField(
        name='ticker',
        field_type='STRING',
        mode='REQUIRED',
        description='Instrument identifier. Example: "ICFZ18". Length: 50.'
    ),
    SchemaField(
        name='bar_time',
        field_type='TIME',
        mode='REQUIRED',
        description='The time that the bar begins.'
    ),
    SchemaField(
        name='volume',
        field_type='NUMERIC',
        mode='REQUIRED',
        description='Volume delta, Buy Volume - Sell Volume.'
    ),
    SchemaField(
        name='traded_quantity',
        field_type='NUMERIC',
        mode='REQUIRED',
        description='Traded quantity delta, Buy Quantity - Sell Quantity.'
    ),
    SchemaField(
        name='count',
        field_type='NUMERIC',
        mode='REQUIRED',
        description='Count trades delta, Buy Trades - Sell Trades.'
    ),
    SchemaField(
        name='buy_volume',
        field_type='NUMERIC',
        mode='NULLABLE',
        description='Buy Volume, Price * Quantity.'
    ),
    SchemaField(
        name='buy_traded_quantity',
        field_type='NUMERIC',
        mode='NULLABLE',
        description='Total buy traded quantity.'
    ),
    SchemaField(
        name='buy_count',
        field_type='NUMERIC',
        mode='NULLABLE',
        description='Count of buy trades (row count for this bar).'
    ),
    SchemaField(
        name='sell_volume',
        field_type='NUMERIC',
        mode='NULLABLE',
        description='Sell Volume, Price * Quantity.'
    ),
    SchemaField(
        name='sell_traded_quantity',
        field_type='NUMERIC',
        mode='NULLABLE',
        description='Total sell traded quantity.'
    ),
    SchemaField(
        name='sell_count',
        field_type='NUMERIC',
        mode='NULLABLE',
        description='Count of sell trades (row count for this bar).'
    ),
]
