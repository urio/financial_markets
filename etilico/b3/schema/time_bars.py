from google.cloud.bigquery import SchemaField


SCHEMA = [
    SchemaField(
        name='period',
        field_type='STRING',
        mode='REQUIRED',
        description=('Period of time for generating bars.')
    ),
    SchemaField(
        name='session_date',
        field_type='DATE',
        mode='REQUIRED',
        description='Session date. Format: YYYY-MM-dd. Example: 2018-09-20.'
    ),
    SchemaField(
        name='ticker',
        field_type='STRING',
        mode='REQUIRED',
        description='Instrument identifier. Example: "ICFZ18". Length: 50.'
    ),
    SchemaField(
        name='bar_time',
        field_type='TIME',
        mode='REQUIRED',
        description='The time that the bar begins.'
    ),
    SchemaField(
        name='high',
        field_type='NUMERIC',
        mode='REQUIRED',
        description='Highest traded price.'
    ),
    SchemaField(
        name='low',
        field_type='NUMERIC',
        mode='REQUIRED',
        description='Lowest traded price.'
    ),
    SchemaField(
        name='open',
        field_type='NUMERIC',
        mode='REQUIRED',
        description='Opening trade price.'
    ),
    SchemaField(
        name='close',
        field_type='NUMERIC',
        mode='REQUIRED',
        description='Closing trade price.'
    ),
    SchemaField(
        name='median',
        field_type='NUMERIC',
        mode='REQUIRED',
        description='Median traded price.'
    ),
    SchemaField(
        name='mean',
        field_type='NUMERIC',
        mode='REQUIRED',
        description='Average traded price.'
    ),
    SchemaField(
        name='volume',
        field_type='NUMERIC',
        mode='REQUIRED',
        description='Volume, Price * Quantity.'
    ),
    SchemaField(
        name='vwap',
        field_type='NUMERIC',
        mode='REQUIRED',
        description='Volume-weighted average price, Volume / Total Quantity.'
    ),
    SchemaField(
        name='traded_quantity',
        field_type='NUMERIC',
        mode='REQUIRED',
        description='Total traded quantity.'
    ),
    SchemaField(
        name='count',
        field_type='NUMERIC',
        mode='REQUIRED',
        description='Count of trades (row count for this bar).'
    ),
]
