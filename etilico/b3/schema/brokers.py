from google.cloud.bigquery import SchemaField


SCHEMA = [
    SchemaField(
        name='date',
        field_type='DATE',
        mode='REQUIRED',
        description=('Reference date.')
    ),
    SchemaField(
        name='member',
        field_type='INT64',
        mode='REQUIRED',
        description='Member ID for this broker. Example: 174.'
    ),
    SchemaField(
        name='name',
        field_type='STRING',
        mode='REQUIRED',
        description='Name of the broker. Example: "BBM CCVM S.A.".'
    ),
    SchemaField(
        name='url',
        field_type='STRING',
        mode='NULLABLE',
        description='The URL of this broker.'
    ),
]
