"""Time bars tables."""
import logging

import api.bigquery
from . import schema
from .stock import get_stock_table
from .futures import get_futures_table


def insert_query(table_id, source_table_id, date, period_minutes, period_name):
    sql = f'''
INSERT INTO `{table_id}` (
           period, session_date, ticker, bar_time, high, low,
           open, close, median, mean, volume, vwap, traded_quantity, count)
  WITH stock AS (
SELECT session_date,
       ticker,
       trade_time,
       trade_price,
       traded_quantity
  FROM `{source_table_id}`
 WHERE session_date = DATE {date!r}),
       minmax AS (
SELECT TIME_TRUNC(MIN(trade_time), SECOND) AS min_time,
       TIME_TRUNC(MAX(trade_time), SECOND) AS max_time,
       {period_minutes} AS step
  FROM stock),
       intervals AS (
SELECT TIME_ADD(min_time, INTERVAL step * num MINUTE) AS bar_begin_time,
       TIME_ADD(
           min_time,
           INTERVAL step * 60 * (1 + num) - 1 SECOND) AS bar_end_time
  FROM minmax,
       UNNEST(GENERATE_ARRAY(
           0, DIV(TIME_DIFF(max_time, min_time, MINUTE), step))) AS num)
SELECT {period_name!r} AS period,
       a.session_date,
       a.ticker,
       a.bar_begin_time AS bar_time,
       MAX(a.trade_price) AS high,
       MIN(a.trade_price) AS low,
       MIN(CASE
           WHEN a.seq_asc = 1
           THEN trade_price
            END) AS open,
       MIN(CASE
           WHEN a.seq_desc = 1
           THEN trade_price
            END) AS close,
       CAST(MIN(a.median) AS NUMERIC) AS median,
       AVG(a.trade_price) AS mean,
       SUM(a.trade_price * a.traded_quantity) AS volume,
       SUM(a.trade_price * a.traded_quantity) / SUM(a.traded_quantity) AS vwap,
       SUM(a.traded_quantity) AS traded_quantity,
       COUNT(a.trade_time) AS count
  FROM (SELECT session_date,
               ticker,
               bar_begin_time,
               bar_end_time,
               trade_time,
               trade_price,
               traded_quantity,
               ROW_NUMBER() OVER asc_window AS seq_asc,
               ROW_NUMBER() OVER desc_window AS seq_desc,
               PERCENTILE_CONT(trade_price, 0.5) OVER group_window AS median
          FROM intervals
               JOIN
               stock
               ON trade_time BETWEEN bar_begin_time AND bar_end_time
        WINDOW group_window AS (PARTITION BY session_date, ticker, bar_begin_time),
               asc_window AS (PARTITION BY session_date, ticker, bar_begin_time ORDER BY trade_time ASC),
               desc_window AS (PARTITION BY session_date, ticker, bar_begin_time ORDER BY trade_time DESC)) AS a
 GROUP BY a.session_date, a.ticker, a.bar_begin_time
 ORDER BY a.session_date, a.ticker, a.bar_begin_time
    '''
    return sql


def sync(dataset, table, source_table):
    table_id = table.sql_table_id
    small_table_id = table.table_id
    source_table_id = source_table.sql_table_id
    client = table.client

    stock_dates = set(source_table.partitions())
    bar_dates = set(table.partitions())
    missing_dates = sorted(stock_dates - bar_dates)

    logger = logging.getLogger('b3.tick_bars')
    for date in missing_dates:
        date_str = date.strftime('%Y-%m-%d')
        logger.info(f'Processing {small_table_id} {date_str}')
        views = [
            (5, '5 minutes'),
            (15, '15 minutes'),
            (60, '60 minutes'),
            (720, '1 day'),
        ]
        for size, name in views:
            sql = insert_query(table_id, source_table_id, date_str, size, name)
            job = client.query(sql)
            job.result()


def sync_stock(dataset):
    table = api.bigquery.Table(
        name='stock_time_bars',
        description=(
            'Historical Bovespa Stock Exchange trades time bars.\n'
            'Availability: D-2 trading days.\n'
            'Key: (period, session_date, ticker, bar_time)'
        ),
        labels={
            'source': 'b3',
            'environment': 'production',
            'component': 'ingest',
            'team': 'data',
            'state': 'active',
        },
        schema=schema.time_bars.SCHEMA,
        partition='session_date',
        dataset=dataset
    )
    table.sync()
    source_table = get_stock_table(dataset)
    sync(dataset, table, source_table)


def sync_futures(dataset):
    table = api.bigquery.Table(
        name='futures_time_bars',
        description=(
            'Historical Bovespa Futures Stock Exchange trades time bars.'
            'Availability: D-2 trading days.\n'
            'Key: (period, session_date, ticker, bar_time)'
        ),
        labels={
            'source': 'b3',
            'environment': 'production',
            'component': 'ingest',
            'team': 'data',
            'state': 'active',
        },
        schema=schema.time_bars.SCHEMA,
        partition='session_date',
        dataset=dataset
    )
    table.sync()
    source_table = get_futures_table(dataset)
    sync(dataset, table, source_table)
