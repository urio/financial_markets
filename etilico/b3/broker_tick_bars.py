"""Tick bars tables."""
import logging

import api.bigquery
from . import schema
from .stock import get_stock_table
from .futures import get_futures_table


def insert_query(table_id, source_table_id, date, period_ticks, period_name):
    sql = f'''
INSERT INTO `{table_id}` (
           period, session_date, member, ticker, bar_time,
           volume, traded_quantity, count,
           buy_volume, buy_traded_quantity, buy_count,
           sell_volume, sell_traded_quantity, sell_count)
  WITH stock AS (
SELECT session_date,
       ticker,
       DIV(ROW_NUMBER() OVER w1 - 1, {period_ticks}) + 1 AS bar_index,
       buy_member,
       sell_member,
       trade_time,
       trade_price,
       traded_quantity
  FROM `{source_table_id}`
 WHERE session_date = DATE {date!r}
WINDOW w1 AS (PARTITION BY session_date, ticker ORDER BY trade_time)),
       buy_stock AS (
SELECT session_date,
       buy_member AS member,
       ticker,
       bar_index,
       trade_time,
       trade_price,
       traded_quantity
  FROM stock),
       sell_stock AS (
SELECT session_date,
       sell_member AS member,
       bar_index,
       ticker,
       trade_time,
       trade_price,
       traded_quantity
  FROM stock),
       buy_bar AS (
SELECT {period_name!r} AS period,
       session_date,
       member,
       ticker,
       bar_index,
       TIME_TRUNC(MIN(trade_time), SECOND) AS bar_time,
       SUM(trade_price * traded_quantity) AS volume,
       SUM(traded_quantity) AS traded_quantity,
       COUNT(trade_time) AS count
  FROM buy_stock
 GROUP BY session_date, member, ticker, bar_index
 ORDER BY session_date, member, ticker, bar_index),
       sell_bar AS (
SELECT {period_name!r} AS period,
       session_date,
       member,
       ticker,
       bar_index,
       TIME_TRUNC(MIN(trade_time), SECOND) AS bar_time,
       SUM(trade_price * traded_quantity) AS volume,
       SUM(traded_quantity) AS traded_quantity,
       COUNT(trade_time) AS count
  FROM sell_stock
 GROUP BY session_date, member, ticker, bar_index
 ORDER BY session_date, member, ticker, bar_index),
       member_join AS (
SELECT COALESCE(b.period, s.period) AS period,
       COALESCE(b.session_date, s.session_date) AS session_date,
       COALESCE(b.member, s.member) AS member,
       COALESCE(b.ticker, s.ticker) AS ticker,
       COALESCE(b.bar_index, s.bar_index) AS bar_index,
       COALESCE(b.bar_time, s.bar_time) AS bar_time,
       b.volume AS buy_volume,
       b.traded_quantity AS buy_traded_quantity,
       b.count AS buy_count,
       s.volume AS sell_volume,
       s.traded_quantity AS sell_traded_quantity,
       s.count AS sell_count
  FROM buy_bar AS b
       FULL OUTER JOIN
       sell_bar AS s
       ON b.period = s.period
       AND b.session_date = s.session_date
       AND b.member = s.member
       AND b.ticker = s.ticker
       AND b.bar_index = s.bar_index)
SELECT period,
       session_date,
       member,
       ticker,
       bar_time,
       COALESCE(buy_volume, 0) - COALESCE(sell_volume, 0) AS volume,
       COALESCE(buy_traded_quantity, 0)
       - COALESCE(sell_traded_quantity, 0) AS traded_quantity,
       COALESCE(buy_count, 0) - COALESCE(sell_count, 0) AS count,
       buy_volume,
       buy_traded_quantity,
       buy_count,
       sell_volume,
       sell_traded_quantity,
       sell_count
  FROM member_join
 ORDER BY session_date, member, ticker, bar_index
    '''
    return sql


def sync(table, source_table):
    table_id = table.sql_table_id
    small_table_id = table.table_id
    source_table_id = source_table.sql_table_id
    client = table.client

    stock_dates = set(source_table.partitions())
    bar_dates = set(table.partitions())
    missing_dates = sorted(stock_dates - bar_dates)

    logger = logging.getLogger('b3.broker_tick_bars')
    for date in missing_dates:
        date_str = date.strftime('%Y-%m-%d')
        logger.info(f'Processing {small_table_id} {date_str}')
        views = [
            (500, '500 ticks'),
            (1000, '1000 ticks'),
        ]
        for size, name in views:
            sql = insert_query(table_id, source_table_id, date_str, size, name)
            job = client.query(sql)
            job.result()


def sync_stock(dataset):
    table = api.bigquery.Table(
        name='stock_broker_tick_bars',
        description=(
            'Historical Bovespa Stock Exchange trades broker tick bars.\n'
            'Availability: D-2 trading days.\n'
            'Key: (period, session_date, member, ticker, bar_time)'
        ),
        labels={
            'source': 'b3',
            'environment': 'production',
            'component': 'ingest',
            'team': 'data',
            'state': 'active',
        },
        schema=schema.broker_tick_bars.SCHEMA,
        partition='session_date',
        dataset=dataset
    )
    table.sync()
    source_table = get_stock_table(dataset)
    sync(table, source_table)


def sync_futures(dataset):
    table = api.bigquery.Table(
        name='futures_broker_tick_bars',
        description=(
            'Historical Bovespa Futures Stock Exchange broker trades tick bars.'
            'Availability: D-2 trading days.\n'
            'Key: (period, session_date, member, ticker, bar_time)'
        ),
        labels={
            'source': 'b3',
            'environment': 'production',
            'component': 'ingest',
            'team': 'data',
            'state': 'active',
        },
        schema=schema.broker_tick_bars.SCHEMA,
        partition='session_date',
        dataset=dataset
    )
    table.sync()
    source_table = get_futures_table(dataset)
    sync(table, source_table)
