"""Brokers listing table."""
import logging

import api.bigquery
from . import schema


def insert_query(table_id):
    sql = f'''
INSERT INTO `{table_id}` (date, member, name, url)
VALUES
('2012-10-18', 1, 'MAGLIANO S.A. CCVM', 'www.magliano.com.br'),
('2012-10-18', 2, 'SOUZA BARROS CT S.A.', 'www.souzabarros.com.br'),
('2012-10-18', 3, 'XP INVESTIMENTOS CCTVM S.A.', 'www.xpi.com.br'),
('2012-10-18', 4, 'ALFA CCVM S.A.', 'www.alfanet.com.br'),
('2012-10-18', 8, 'UBS BRASIL CCTVM S.A.', 'www.ubs.com/br/pt.html'),
('2012-10-18', 9, 'DEUTSCHE BANK CV S.A.', 'www.db.com/brazil'),
('2012-10-18', 10, 'SPINELLI S.A. CVMC', 'www.spinelli.com.br'),
('2012-10-18', 13, 'MERRILL LYNCH S.A. CTVM', 'www.merrilllynch-brasil.com.br'),
('2012-10-18', 14, 'CRUZEIRO DO SUL S.A. CV', 'www.apregoa.com'),
('2012-10-18', 15, 'INDUSVAL S.A. CTVM', 'www.indusvaltrade.com.br'),
('2012-10-18', 16, 'J.P.MORGAN CCVM S.A.', 'www.jpmorgan.com.br'),
('2012-10-18', 18, 'BBM CCVM S.A.', 'www.bancobbm.com.br'),
('2012-10-18', 21, 'VOTORANTIM CTVM LTDA.', 'www.votorantimcorretora.com.br'),
('2012-10-18', 23, 'CONCORDIA S.A. CVMCC', 'www.concordia.com.br'),
('2012-10-18', 27, 'SANTANDER CCVM S.A.', 'www.santandercorretora.com.br'),
('2012-10-18', 29, 'UNILETRA CCTVM S.A.', 'www.uniletra.com.br'),
('2012-10-18', 33, 'LEROSA S.A. CV', 'www.lerosa.com.br'),
('2012-10-18', 35, 'PETRA PERSONAL TRADER CTVM LTDA.', 'www.petractvm.com.br'),
('2012-10-18', 37, 'UM INVESTIMENTOS S.A. CTVM', 'www.uminvestimentos.com.br'),
('2012-10-18', 38, 'CORVAL CVM S.A.', 'www.corval.com.br'),
('2012-10-18', 39, 'AGORA CTVM S.A.', 'www.agorainvest.com.br'),
('2012-10-18', 40, 'MORGAN STANLEY CTVM S.A.', 'www.morganstanley.com.br'),
('2012-10-18', 41, 'ING CCT S.A.', 'www.ing.com.br'),
('2012-10-18', 45, 'CREDIT SUISSE BRASIL S.A. CTVM', 'br.credit-suisse.com'),
('2012-10-18', 47, 'SOLIDEZ CCTVM LTDA.', 'www.solidez.com.br'),
('2012-10-18', 54, 'BES SECURITIES DO BRASIL CCVM', 'www.bessecurities.com.br'),
('2012-10-18', 57, 'BRASCAN S.A. CTV', 'www.brascanctv.com.br'),
('2012-10-18', 58, 'SOCOPA SC PAULISTA S.A.', 'www.socopa.com.br'),
('2012-10-18', 59, 'J.SAFRA CVC LTDA.', 'www.safracorretora.com.br'),
('2012-10-18', 63, 'NOVINVEST CVM LTDA.', 'www.topbroker.com.br'),
('2012-10-18', 70, 'HSBC CTVM S.A.', 'www.hsbcinvestimentos.com.br'),
('2012-10-18', 72, 'BRADESCO S.A. CTVM', 'www.shopinvest.com.br'),
('2012-10-18', 74, 'COINVALORES CCVM LTDA.', 'www.coinvalores.com.br'),
('2012-10-18', 75, 'TALARICO CCTM LTDA.', 'www.talaricocctm.com.br'),
('2012-10-18', 76, 'INTERBOLSA DO BRASIL CCTVM', 'www.interbolsa.com.br'),
('2012-10-18', 77, 'CITIGROUP GMB CCTVM S.A.', 'www.citicorretora.com.br'),
('2012-10-18', 82, 'TOV CCTVM LTDA.', 'www.tov.com.br'),
('2012-10-18', 83, 'MAXIMA S.A. CTVM', 'www.maximatrade.com.br'),
('2012-10-18', 85, 'BTG PACTUAL CTVM S.A.', 'www.btgpactual.com'),
('2012-10-18', 86, 'WALPIRES S.A. CCTVM', 'www.walpires.com.br'),
('2012-10-18', 88, 'CM CAPITAL MARKETS CCTVM LTDA.', 'www.cmcapitalmarkets.com.br'),
('2012-10-18', 90, 'TITULO CV S.A.', 'www.easyinvest.com.br'),
('2012-10-18', 92, 'RENASCENCA DTVM LTDA.', 'www.renatrader.com.br'),
('2012-10-18', 93, 'NOVA FUTURA CTVM LTDA', 'www.futuraonline.com.br'),
('2012-10-18', 95, 'CS HEDGING-GRIFFO CV S.A.', 'www.cshg.com.br'),
('2012-10-18', 98, 'ALPES CCTVM LTDA.', 'www.wintrade.com.br'),
('2012-10-18', 99, 'TENDENCIA CCTVM LTDA.', 'www.tendencia.com.br'),
('2012-10-18', 102, 'CGD INVESTIMENTOS CVC S.A.', 'www.banifinvest.com.br'),
('2012-10-18', 104, 'DISTRIBUIDORA INTERCAP TVM S.A.', 'www.intercap.com.br'),
('2012-10-18', 106, 'MERCANTIL DO BRASIL CORRETORA S.A. CTVM', 'corretora.mercantildobrasil.com.br'),
('2012-10-18', 107, 'TERRA INVESTIMENTOS CM S.A.', 'www.terrafuturos.com.br'),
('2012-10-18', 110, 'SLW CVC LTDA.', 'www.slw.com.br'),
('2012-10-18', 114, 'ITAU CV S.A.', 'www.itautrade.com.br'),
('2012-10-18', 115, 'H.COMMCOR DTVM LTDA.', 'www.commcor.com.br'),
('2012-10-18', 119, 'BTG PACTUAL CM LTDA.', 'www.btgpactual.com'),
('2012-10-18', 120, 'BRASIL PLURAL CCTVM S.A.', 'www.brasilpluralcorretora.com'),
('2012-10-18', 122, 'BCG LIQUIDEZ DTVM', 'www.bgcliquidez.com'),
('2012-10-18', 127, 'TULLETT PREBON BRASIL S.A. CVC', 'www.convencao.com.br'),
('2012-10-18', 129, 'PLANNER CV S.A.', 'www.planner.com.br'),
('2012-10-18', 131, 'FATOR S.A. CV', 'www.fatorcorretora.com.br'),
('2012-10-18', 133, 'DIBRAN DTVM LTDA.', 'www.dibran.com.br'),
('2012-10-18', 134, 'BARCLAYS CTVM S.A.', 'www.barcap.com/brasil/'),
('2012-10-18', 140, 'DIFERENCIAL CTVM S.A.', 'www.diferencial.com.br'),
('2012-10-18', 147, 'ATIVA S.A. CTCV', 'www.invistaativa.com.br'),
('2012-10-18', 157, 'ESCRITORIO RUY LAGE SCT LTDA.', 'www.corretoraruylage.com.br'),
('2012-10-18', 172, 'BANRISUL S.A. CVMC', 'www.banrisulcorretora.com.br'),
('2012-10-18', 173, 'GERACAO FUTURO CV S.A.', 'www.gerafuturo.com.br'),
('2012-10-18', 174, 'ELITE CCVM LTDA.', 'www.eliteccvm.com.br'),
('2012-10-18', 175, 'OMAR CAMARGO CCV LTDA.', 'www.omarcamargo.com.br'),
('2012-10-18', 177, 'SOLIDUS S.A. CCVM', 'www.solidus.com.br'),
('2012-10-18', 186, 'GERAL DE VC LTDA.', 'www.geralinvestimentos.com.br'),
('2012-10-18', 187, 'SITA SCCVM S.A.', 'www.sita.com.br'),
('2012-10-18', 189, 'PRIME S.A. CCV', 'www.primecorretora.com.br'),
('2012-10-18', 190, 'PILLA CVMC LTDA.', 'www.pilla.com.br'),
('2012-10-18', 191, 'SENSO CCVM S.A.', 'www.sensocorretora.com.br'),
('2012-10-18', 192, 'GERALDO CORREA CVM S.A.', 'www.geraldocorrea.com.br'),
('2012-10-18', 226, 'AMARIL FRANKLIN CTV LTDA.', 'www.amarilfranklin.com.br'),
('2012-10-18', 227, 'GRADUAL CCTVM LTDA.', 'www.gradualinvestimentos.com.br'),
('2012-10-18', 228, 'UNIBANCO INVESTSHOP CVMC S.A.', 'www.investshop.com.br'),
('2012-10-18', 232, 'H.H.PICCHIONI S.A. CCVM', 'www.picchioni.com.br'),
('2012-10-18', 234, 'CODEPE CV S.A.', 'www.codepe.com.br'),
('2012-10-18', 237, 'OLIVEIRA FRANCO SCVC LTDA.', 'www.ofranco.com.br'),
('2012-10-18', 238, 'GOLDMAN SACHS DO BRASIL CTVM', 'www.gs.com'),
('2012-10-18', 262, 'MIRAE ASSET SECURITIES BRASIL CCTVM LTDA', 'corretora.miraeasset.com.br'),
('2012-10-18', 298, 'CITIBANK DTVM S.A.', null),
('2012-10-18', 308, 'CLEAR CTVM LTDA.', 'www.clear.com.br'),
('2012-10-18', 386, 'OCTO CTVM S.A.', 'www.rico.com.vc'),
('2012-10-18', 734, 'GBM BRASIL DTVM S.A.', 'www.gbms.com.br'),
('2012-10-18', 735, 'ICAP DO BRASIL CTVM LTDA.', 'www.mycap.com.br'),
('2012-10-18', 2252, 'FUTURA COMMODITIES CM LTDA.', 'www.futuraonline.com.br'),
('2012-10-18', 3130, 'FLOW CM', 'www.brasilpluralcorretora.com'),
('2012-10-18', 3449, 'PROSPER APLICACAO CMF LTDA.', 'www.prospercorretora.com.br');
    '''
    return sql


def sync(dataset):
    table = api.bigquery.Table(
        name='brokers',
        description=(
            'Listing of brokers by member id and name.\n'
            'Source: http://apligraf.com.br/codigos-corretoras\n'
            'Availability: Static information, indexed by reference date.'
        ),
        labels={
            'source': 'hardcoded',
            'environment': 'production',
            'component': 'ingest',
            'team': 'data',
            'state': 'active',
        },
        schema=schema.brokers.SCHEMA,
        partition=None,
        dataset=dataset
    )
    table.sync()
    if table.get_object().num_rows == 0:
        logger = logging.getLogger('b3.brokers')
        logger.info('Populating brokers table.')
        sql = insert_query(table.sql_table_id)
        table.client.query(sql).result()
