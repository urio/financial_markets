"""Futures exchange table."""
import gc
import logging
import re
from datetime import datetime

import api.bigquery
from . import schema
from .reader import STOCK_COLS, read_stock


STOCK_PATH = 'datalake/stock_market/b3/MarketData/BMF'
"""Bucket location of all stock files"""

STOCK_NAME_PATTERN = re.compile(r'^NEG_BMF_\d{8}\.(zip|gz)$')
"""Pattern that matches NEG_YYYYMMdd.zip"""

DATE_PATTERN = re.compile(r'\d{8}')
"""Pattern that matches YYYYMMdd in the file name"""


def extract_date_from_path(path):
    date = re.findall(DATE_PATTERN, path)[0]
    return datetime.strptime(date, '%Y%m%d').date()


def get_futures_table(dataset):
    return api.bigquery.Table(
        name='futures',
        description=(
            'Historical Bovespa Stock Futures Exchange trades.\n'
            'Source: ftp://ftp.bmf.com.br/MarketData\n'
            'Availability: D-2 trading days.'
        ),
        labels={
            'source': 'ftp',
            'environment': 'production',
            'component': 'ingest',
            'team': 'data',
            'state': 'active',
        },
        schema=schema.stock.SCHEMA,
        partition='session_date',
        dataset=dataset
    )


def sync(bucket, dataset, stage_dataset):
    table = get_futures_table(dataset)
    buffer_table = api.bigquery.Table(
        name='futures_buffer',
        description='Temporary buffer for stock ingestion.',
        labels=dict(),
        schema=schema.stock.BUFFER_SCHEMA,
        partition=None,
        dataset=stage_dataset
    )

    table.sync()

    client = table.client
    table_id = table.sql_table_id
    buffer_id = buffer_table.sql_table_id

    logger = logging.getLogger('b3.futures')

    dates = set(table.partitions())

    files_to_process = list()
    for blob in bucket.files(STOCK_PATH, STOCK_NAME_PATTERN):
        date = extract_date_from_path(blob.name)
        if date not in dates:
            files_to_process.append(blob)

    if not files_to_process:
        logger.info('Up-to-date.')
        return

    # Create buffer table.
    buffer_table.sync()

    for blob in files_to_process:
        logger.info(f'Processing {blob.name}')

        logger.debug('Cleaning up buffer.')
        sql = f'DELETE FROM `{buffer_id}` WHERE true'
        job = client.query(sql, location=table.dataset.location)
        job.result()

        logger.debug('Downloading and reading.')
        data = read_stock(blob)

        logger.debug('Sending to BigQuery.')
        data = data[STOCK_COLS]
        buffer_table.upload(data)

        del data
        gc.collect()

        sql = f'''
INSERT INTO `{table_id}` (
       row_number, session_date, ticker, trade_number, trade_price,
       traded_quantity, trade_time, trade_ind, aggr_buy_order_ind,
       aggr_sell_order_ind, cross_trade_ind, buy_member, sell_member)
SELECT CAST(row_number AS INT64),
       CAST(session_date AS DATE),
       ticker,
       CAST(trade_number AS INT64),
       CAST(trade_price AS NUMERIC),
       CAST(traded_quantity AS INT64),
       CAST(trade_time AS TIME),
       CAST(trade_ind AS INT64),
       CAST(aggr_buy_order_ind AS INT64),
       CAST(aggr_sell_order_ind AS INT64),
       CAST(cross_trade_ind AS INT64),
       CAST(buy_member AS INT64),
       CAST(sell_member AS INT64)
  FROM `{buffer_id}`
 ORDER BY CAST(row_number AS INT64);
        '''
        query_job = client.query(sql, location=table.dataset.location)
        query_job.result()

    buffer_table.delete()
    logger.info('Done')
