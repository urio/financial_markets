"""Tick bars tables."""
import logging

import api.bigquery
from . import schema
from .stock import get_stock_table
from .futures import get_futures_table


def insert_query(table_id, source_table_id, date, period_ticks, period_name):
    sql = f'''
INSERT INTO `{table_id}` (
           period, session_date, ticker, bar_time, high, low,
           open, close, median, mean, volume, vwap, traded_quantity, count)
  WITH stock AS (
SELECT session_date,
       ticker,
       DIV(ROW_NUMBER() OVER w1 - 1, {period_ticks}) + 1 AS bar_index,
       trade_time,
       trade_price,
       traded_quantity
  FROM `{source_table_id}`
 WHERE session_date = DATE {date!r}
WINDOW w1 AS (PARTITION BY session_date, ticker ORDER BY trade_time)),
       stock_group AS (
SELECT session_date,
       ticker,
       bar_index,
       trade_time,
       trade_price,
       traded_quantity,
       ROW_NUMBER() OVER asc_window AS seq,
       ROW_NUMBER() OVER asc_window AS seq_asc,
       ROW_NUMBER() OVER desc_window AS seq_desc,
       PERCENTILE_CONT(trade_price, 0.5) OVER group_window AS median
  FROM stock
WINDOW group_window AS (PARTITION BY session_date, ticker, bar_index),
       asc_window AS (PARTITION BY session_date, ticker, bar_index ORDER BY trade_time ASC),
       desc_window AS (PARTITION BY session_date, ticker, bar_index ORDER BY trade_time DESC))
SELECT {period_name!r} AS period,
       session_date,
       ticker,
       TIME_TRUNC(MIN(trade_time), SECOND) AS bar_time,
       MAX(trade_price) AS high,
       MIN(trade_price) AS low,
       MIN(CASE
           WHEN seq_asc = 1
           THEN trade_price
            END) AS open,
       MIN(CASE
           WHEN seq_desc = 1
           THEN trade_price
            END) AS close,
       CAST(MIN(median) AS NUMERIC) AS median,
       AVG(trade_price) AS mean,
       SUM(trade_price * traded_quantity) AS volume,
       SUM(trade_price * traded_quantity) / SUM(traded_quantity) AS vwap,
       SUM(traded_quantity) AS traded_quantity,
       COUNT(seq) AS count
  FROM stock_group
 GROUP BY session_date, ticker, bar_index
 ORDER BY session_date, ticker, bar_index;
    '''
    return sql


def sync(table, source_table):
    table_id = table.sql_table_id
    small_table_id = table.table_id
    source_table_id = source_table.sql_table_id
    client = table.client

    stock_dates = set(source_table.partitions())
    bar_dates = set(table.partitions())
    missing_dates = sorted(stock_dates - bar_dates)

    logger = logging.getLogger('b3.tick_bars')
    for date in missing_dates:
        date_str = date.strftime('%Y-%m-%d')
        logger.info(f'Processing {small_table_id} {date_str}')
        views = [
            (500, '500 ticks'),
            (1000, '1000 ticks'),
        ]
        for size, name in views:
            sql = insert_query(table_id, source_table_id, date_str, size, name)
            job = client.query(sql)
            job.result()


def sync_stock(dataset):
    table = api.bigquery.Table(
        name='stock_tick_bars',
        description=(
            'Historical Bovespa Stock Exchange trades tick bars.\n'
            'Availability: D-2 trading days.\n'
            'Key: (period, session_date, ticker, bar_time)'
        ),
        labels={
            'source': 'b3',
            'environment': 'production',
            'component': 'ingest',
            'team': 'data',
            'state': 'active',
        },
        schema=schema.tick_bars.SCHEMA,
        partition='session_date',
        dataset=dataset
    )
    table.sync()
    source_table = get_stock_table(dataset)
    sync(table, source_table)


def sync_futures(dataset):
    table = api.bigquery.Table(
        name='futures_tick_bars',
        description=(
            'Historical Bovespa Futures Stock Exchange trades tick bars.'
            'Availability: D-2 trading days.\n'
            'Key: (period, session_date, ticker, bar_time)'
        ),
        labels={
            'source': 'b3',
            'environment': 'production',
            'component': 'ingest',
            'team': 'data',
            'state': 'active',
        },
        schema=schema.tick_bars.SCHEMA,
        partition='session_date',
        dataset=dataset
    )
    table.sync()
    source_table = get_futures_table(dataset)
    sync(table, source_table)
