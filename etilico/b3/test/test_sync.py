from unittest import TestCase
from unittest.mock import Mock, patch

from b3.stock import sync


class TestSync(TestCase):

	def test_sync(self):
		database = Mock()
		database.tables.return_value = []
		database.exists.return_value = True
		bucket = Mock()
		bucket.files.return_value = []
		sync(bucket, database)
