from unittest import TestCase
from unittest.mock import patch, Mock
from collections import namedtuple

from etilico.api.bigquery import Dataset, Table


MockDataset = namedtuple('MockDataset', ['dataset_id'])


class TestDataset(TestCase):

    def test_dataset_init(self):
        client = Mock()
        client.list_datasets.return_value = [MockDataset('dataset')]
        dataset = Dataset('dataset', 'description', dict(), 'us', client)


class TestTable(TestCase):

    def test_table_init(self):
        client = Mock()
        client.list_datasets.return_value = [MockDataset('table')]
        client.list_tables.return_value = []
        dataset = Dataset('dataset', 'description', dict(), 'us', client)
        table = Table('table', 'description', dict(), None, None, dataset)
