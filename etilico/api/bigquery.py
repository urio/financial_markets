"""Google API utilities."""
import logging
from abc import ABC, abstractmethod
from datetime import datetime
from tempfile import NamedTemporaryFile

from google.cloud import bigquery
from google.cloud.bigquery.table import TimePartitioning
from google.cloud.bigquery.job import LoadJobConfig
from google.cloud.bigquery.job import SourceFormat, WriteDisposition


class SynchronizeError(Exception):
    """Base class synchronization errors."""
    pass


class SynchronizedMetadata(object):
    """
    Metadata synchronization control.

    The objects we're working with accept the same metadata information.
    """

    METADATA_MATCHING = {
        'description': lambda curr, exp: curr == exp,
        'labels': lambda curr, exp: not (curr.items() ^ exp.items())
    }

    def __init__(self, description: str, labels: dict):
        assert isinstance(description, str) or description is None
        assert isinstance(labels, dict) or labels is None
        self._description = description
        self._labels = labels

    def _out_of_date_fields(self):
        obj = self.get_object()
        fields = list()
        for field, match in self.METADATA_MATCHING.items():
            current = getattr(obj, field)
            expected = getattr(self, field)
            same_type = type(current) is type(expected)
            if not (same_type and match(current, expected)):
                fields.append(field)
        return fields

    def _update(self, fields):
        obj = self.get_object()
        for field in fields:
            setattr(obj, field, getattr(self, field))
        self.update(obj, fields)

    @property
    def description(self):
        return self._description

    @property
    def labels(self):
        return self._labels

    @abstractmethod
    def get_object(self):
        """Return an existing remote object."""
        pass

    @abstractmethod
    def update(self, object, fields):
        """Update remote metadata."""
        pass

    def sync(self):
        """Ensure dataset exists and that its metadata is up-to-date."""
        fields = self._out_of_date_fields()
        if not fields:
            return
        logger = logging.getLogger('api.bigquery')
        logger.info(f'sync(): Updating {", ".join(fields)} in {self!s}')
        self._update(fields)
        # Check whether the object has been updated or not.
        fields = self._out_of_date_fields()
        if fields:
            errors = ', '.join(fields)
            raise SynchronizeError(
                f'Failed to update the following fields: {errors}.')


class Dataset(SynchronizedMetadata):

    def __init__(self, name: str, description: str, labels: dict,
                 location: str, client: bigquery.Client):
        super().__init__(description, labels)
        self.name = name
        self.location = location
        self.client = client

    def __str__(self):
        return f'Dataset({self.name!r})'

    def _create(self):
        logger = logging.getLogger('api.bigquery')
        logger.info(f'Creating {self.name!r} dataset.')
        ds_ref = self.client.dataset(self.name)
        ds_obj = bigquery.Dataset(ds_ref)
        ds_obj.description = self.description
        ds_obj.labels = self.labels
        return self.client.create_dataset(ds_obj)

    def datasets(self) -> list:
        return [ds.dataset_id for ds in self.client.list_datasets()]

    def tables(self) -> list:
        table_list = self.client.list_tables(self.get_object())
        return [table.table_id for table in table_list]

    @property
    def exists(self):
        return self.name in self.datasets()

    def get_object(self):
        if not self.exists:
            return self._create()
        dataset_ref = self.client.dataset(self.name)
        return self.client.get_dataset(dataset_ref)

    def update(self, obj, fields):
        logger = logging.getLogger('api.bigquery')
        logger.info('Updating dataset')
        logger.info(f'{obj!r}.{fields!r}')
        self.client.update_dataset(obj, fields)


class Table(SynchronizedMetadata):

    def __init__(self, name: str, description: str, labels: dict,
                 schema: list, partition: str,
                 dataset: bigquery.Table):
        super().__init__(description, labels)
        self.name = name
        self.schema = schema
        self.dataset = dataset
        self.client = dataset.client
        if partition:
            self.partition = True
            self.time_partitioning = TimePartitioning(field=partition)
        else:
            self.partition = False

    def __str__(self):
        return f'Table({self.name!r}, dataset={self.dataset!s})'

    def _create(self):
        logger = logging.getLogger('api.bigquery')
        logger.info(f'Creating {self.name!r} table.')
        ds_ref = self.dataset.get_object().table(self.name)
        ds_obj = bigquery.Table(ds_ref, schema=self.schema)
        ds_obj.description = self.description
        ds_obj.labels = self.labels
        if self.partition:
            ds_obj.time_partitioning = self.time_partitioning
        return self.client.create_table(ds_obj)

    @property
    def exists(self):
        return self.name in self.dataset.tables()

    @property
    def sql_table_id(self):
        full_id = self.get_object().full_table_id
        return '.'.join(full_id.split(':'))

    @property
    def table_id(self):
        return self.get_object().table_id

    def delete(self):
        table_ref = self.get_ref()
        self.client.delete_table(table_ref)

    def get_object(self):
        if not self.exists:
            return self._create()
        ds_obj = self.dataset.get_object()
        table_ref = ds_obj.table(self.name)
        return self.client.get_table(table_ref)

    def get_ref(self):
        ds_obj = self.dataset.get_object()
        table_ref = ds_obj.table(self.name)
        return table_ref

    def partitions(self):
        plist = self.client.list_partitions(self.get_ref())
        return [datetime.strptime(name, '%Y%m%d').date()
                for name in plist if name != '__NULL__']

    def update(self, obj, fields):
        self.client.update_table(obj, fields)

    def upload(self, data):
        logger = logging.getLogger('api.bigquery')
        # Current implementation of `load_table_from_dataframe` in
        # the bigquery API uses parquet the forces some coercion, making
        # it impossible to upload all-string data types.  Thus, we
        # adopt a upload from csv method.
        with NamedTemporaryFile(mode='w',
                                encoding='UTF-8',
                                prefix='df_to_bigquery_',
                                suffix='.csv') as writebuf:
            data.to_csv(writebuf, index=False, header=False, encoding='utf-8')
            writebuf.flush()

            with open(writebuf.name, mode='rb') as readbuf:
                job_config = LoadJobConfig()
                job_config.autodetect = False
                job_config.schema = self.schema
                job_config.write_disposition = WriteDisposition.WRITE_TRUNCATE
                job_config.source_format = SourceFormat.CSV
                job = self.client.load_table_from_file(readbuf, self.get_ref())
                job.result()
