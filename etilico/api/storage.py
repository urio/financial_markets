import os
import re

from google.cloud import storage


class Bucket:

    def __init__(self, name: str, client: storage.Client):
        self.client = client
        self.bucket = self.client.get_bucket(name)

    def files(self, path: str, pattern: re.Pattern):
        """List files in a path matching a pattern."""
        for blob in self.bucket.list_blobs(prefix=path):
            if pattern.match(os.path.basename(blob.name)):
                yield blob
